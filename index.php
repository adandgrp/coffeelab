<html>
<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic|Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/mainx.css">
    <link rel="stylesheet" href="css/watch.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Fascinate&display=swap" rel="stylesheet">
    
    <title>Newspaper Style Design Experiment</title>
    <meta name="viewport" content="width=device-width">

</head>
<body style="width:300px;background-image:url('images/bandera.gif'); background-size: cover;height: 100%;">

<?php

header("Refresh: 43200" );

?>
<?php
 
function CLIMA($latitud,$longitud,$dato)
{ $url = "http://api.openweathermap.org/data/2.5/weather?lat=".$latitud."&lon=".$longitud."&appid=bafc5422e83633cf8e663d8a0eb0827d&units=metric";

 $clima = file_get_contents($url);
 $json = json_decode($clima);
   $temperatura = $json->main->temp;
    $temperaturamax = $json->main->temp_max;
     $temperaturamin = $json->main->temp_min;
     $presion = $json->main->pressure;
    $humedad = $json->main->humidity;
 /*    $viento = $json->main->speed;*/
    switch($dato){
      case 1:
        return $temperatura;
      break;  
      case 2:
        return $temperaturamax;
      break;  
      case 3:
        return $temperaturamin;
      break;  
      case 4:
        return $presion;
      break;  
      case 5:
        return $humedad;
      break;  
    }

 return $temperatura;
}

?>
<div class="head">
   <!-- <div class="headerobjectswrapper">
        <div class="weatherforcastbox"><span style="font-style: italic;">Weatherforcast for the next 24 hours: Plenty of Sunshine</span><br><span>Wind: 7km/h SSE; Ther: 21°C; Hum: 82%</span></div>
        <header>Newpost York</header>
    </div> -->

    <div class="subhead" style="left:250px;" id="fechapleca" ></div>

    <div style="margin-left:38px;margin-top:15px;margin-bottom:15px;" id="liveclock" class="outer_face">
        <div class="marker oneseven"></div>
        <div class="marker twoeight"></div>
        <div class="marker fourten"></div>
        <div class="marker fiveeleven"></div>
        <div class="inner_face">
          <div class="hand hour"></div>
          <div class="hand minute"></div>
          <div class="hand second"></div>
        </div>
    </div>
  
  
    <div class="subhead" style="left:250px;top:80px" >Temp. actual <?php echo CLIMA(19.4284700,-99.1276600,1);  ?></div>
</div>
  <div class="subhead" style="left:250px;top:120px" >Max.-&nbsp; <?php echo CLIMA(19.4284700,-99.1276600,2);  ?> &nbsp; Min.-&nbsp; <?php echo CLIMA(19.4284700,-99.1276600,3);  ?></div>
  <div class="subhead" style="left:250px;top:160px" >&nbsp; Humedad.-<?php echo CLIMA(19.4284700,-99.1276600,5);  ?>%</div>
  <div class="subhead" style="left:250px;" ><span  class="headline hl4" id="showusd"></span></div>

</div>

</body>
<script>

exchange();

var $hands = $('#liveclock div.hand')

 

window.requestAnimationFrame = window.requestAnimationFrame

                               || window.mozRequestAnimationFrame

                               || window.webkitRequestAnimationFrame

                               || window.msRequestAnimationFrame

                               || function(f){setTimeout(f, 60)}

 

 

function updateclock(){

	var curdate = new Date()

	var hour_as_degree = ( curdate.getHours() + curdate.getMinutes()/60 ) / 12 * 360

	var minute_as_degree = curdate.getMinutes() / 60 * 360

	var second_as_degree = ( curdate.getSeconds() + curdate.getMilliseconds()/1000 ) /60 * 360

	$hands.filter('.hour').css({transform: 'rotate(' + hour_as_degree + 'deg)' })

	$hands.filter('.minute').css({transform: 'rotate(' + minute_as_degree + 'deg)' })

	$hands.filter('.second').css({transform: 'rotate(' + second_as_degree + 'deg)' })

   var d = new Date(); 
   var yyyy = d.getFullYear();
   var month = d.getMonth()+1;
   var monthletter = "";
    switch(month){
      case 1:
        monthletter  = "Enero";
      break; 
      case 2:
        monthletter  = "Febrero";
      break;
      case 3:
        monthletter  = "Marzo";
      break;
      case 4:
        monthletter  = "Abril";
      break;
      case 5:
        monthletter  = "Mayo";
      break;
      case 6:
        monthletter  = "Junio";
      break;
      case 7:
        monthletter  = "Julio";
      break;
      case 8:
        monthletter  = "Agosto";
      break;
      case 9:
        monthletter  = "Septiembre";
      break;
      case 10:
        monthletter  = "Octubre";
      break;
      case 11:
        monthletter  = "Noviembre";
      break;
      case 12:
        monthletter  = "Diciembre";
      break;

    }


   var day = d.getDate();
    $("#fechapleca").empty();
    $("#fechapleca").append("Hoy es:" +  day +  " de " + monthletter + " del "+   yyyy);



	requestAnimationFrame(updateclock)

}

 

requestAnimationFrame(updateclock)



  

function exchange(){
    debugger;
    fetch(`https://v6.exchangerate-api.com/v6/5876806a30d4c3d52b245cb6/latest/USD`)
    .then(res => res.json())
    .then(res => {
        debugger;
        
        const new_rate = res['conversion_rates']['MXN'];
        $("#showusd").empty();
        $("#showusd").append("USD: $" + new_rate);
        debugger;
    })

   
    
}


</script>  

</html>